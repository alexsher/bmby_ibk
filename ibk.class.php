<?php
/**
* Class of Book-keeping Interface Dispatcher for BMBY.Manage
* @version 1.0.0
* 
* @since 05 May, 2009	- Alex Sherman 		- Initial version
* 
* @author Alex Sherman [<alex@sherman-site.com>]
*/
require_once(INCLUDE_PATH ."lib/common.class.php");
class CIbk {
 
	const ERR_GENERAL				= 999;
	//Main Dispatcher Faults
	const ERR_NO_DB 				= 100;
	const ERR_PROJECT_TOKEN_ERROR 	= 110;
	const ERR_IP_ADDRESS_REJECTED 	= 115;
	const ERR_UNKNOWN_METHOD	    = 150;
	
	//Handler Faults - DATA
	const ERR_WRONG_DOCUMENT_TYPE	= 300;
	const ERR_WRONG_DATE_FORMAT		= 310;
	const ERR_WRONG_BOOKKEEPING_ID	= 320;
	const ERR_WRONG_CONTRACT_ID		= 330;
	const ERR_WRONG_CURRENCY_ID		= 340;
	const ERR_WRONG_POSITIVE_SUM	= 350;
	const ERR_WRONG_CURRENCY_RATE	= 360;
	const ERR_REFRENCE_REQUIRED		= 370;
	const ERR_REFERENCE_IN_USE		= 380;
	
	const ERR_WRONG_PAYMENT_TYPE_ID = 410;
	const ERR_NOT_VALID_RECEIPT_ID	= 420;
	const ERR_PAYMENT_EXCEEDS_RCPT  = 430;
	
	//Handler Faults - DB
	const ERR_FAILED_TO_WRITE		= 500;
	
	/**
	* @var array
	*/
	public static $arrErrMsg = array(
		self::ERR_GENERAL 		=> "General Error",
		self::ERR_NO_DB 		=> "No Database Connection",
		110 => "Project Token Error",
		115 => "IP Address Rejected",
		150 => "Unknown Method",
		300 => "Wrong Document Type",
		310 => "Wrong Date Format",
		320 => "Wrong Bookkeeping Identifier",
		330 => "Wrong Contract Identifier",
		340 => "Wrong Currency Identifier",
		350 => "Sum Must Be Greater Than Zero",
		360 => "Currency Rate Must Be Greater Than Zero",
		370 => "Reference to 3rd Party Software Required",
		380 => "Reference Already In Use",
		410 => "Wrong Payment Type Identifier",
		420 => "Not Valid Receipt Identifier",
		430 => "Payment Sum Exceeds Total Receipt Sum",
		500 => "Failed To Write Data to Database"
	);	
	
	public static $strEncoding;
	/**
	* @var Zend_Db_Adapter_Abstract
	*/
	protected $zdb;
	
	/**
	* @var int
	*/
	private $intIp;
	
	/**
	* @var string
	*/
	private $strDebug = "";
	
	/**
	* @var CBookKeepingInterface
	*/
	protected $objHandler;
	
	/**
	* @var string
	*/
	protected $strHandlerClass = "CBookKeepingInterfaceHandler";

	/**
	* @return  CBookKeepingInterface
	*/	
	public function __construct() {
		$this->zdb = Zend_Registry::get('zdb');
		if (!($this->zdb instanceof Zend_Db_Adapter_Abstract)) {
			throw new SoapFault('Server',self::fnGetError(self::ERR_NO_DB));
		}
	}
    
    /**
    * put your comment there...
    * 
    * @param string $pstrMethod
    * @param array $parrParams
    * @return mixed
    */
  	public function __call($pstrMethod, $parrParams=null) {
  		$strToken = array_shift($parrParams);
  		$this->fnAuthenticate($strToken);
  		$strMethodName = "fn".$pstrMethod;                             
  		if (class_exists($this->strHandlerClass) && method_exists($this->strHandlerClass,$strMethodName)) {
  			//Convert Input from UTF8
  			array_walk_recursive($parrParams,array($this,'fnConvertInputEncoding'));
  			//Call handler defined method   
  			$mixResult = call_user_func_array(array($this->strHandlerClass,$strMethodName),$parrParams);
  			//Convert Output to UTF8
  			array_walk_recursive($mixResult,array($this,'fnConvertOutputEncoding'));
  			return $mixResult;
		} else {
			throw new SoapFault('Server',self::fnGetError(self::ERR_UNKNOWN_METHOD));
		}
	}		

 	/**
	* Check authentication token and IP of the client vs Project Settings
	* 
	* @param string $strToken
	* @return void
	*/
	private function fnAuthenticate($strToken) {
		$strProjectToken = substr($strToken,0,32); //Get Project Token out of Token string
		$intProjectId = (int)substr($strToken,32); //Get ProjectID out of Token string
		$arrResult = $this->zdb->fetchRow("SELECT P.*,L.Encoding FROM ".CCommon::strTableIbkProjects." P LEFT JOIN ".CCommon::strTableLanguages." L ON P.DLangID = L.LangID WHERE P.Active = 1 AND P.ProjectID = {$intProjectId} AND P.Token = ".$this->zdb->quote($strProjectToken));
		if (!empty($arrResult)) {
			Zend_Registry::set('PID',$intProjectId);
			self::$strEncoding = $arrResult['Encoding'];
			//Validate IP access
			if (!(bool)$arrResult['isAnyIpAccess']) {
				$this->fnGetIp();
				$boolIsAllowedIp = (bool)$this->zdb->fetchOne("SELECT id FROM ".CCommon::strTableIbkAllowedAddresses." WHERE ProjectID = {$intProjectId} and fromIP <= {$this->intIp} and toIP >= {$this->intIp}");
				if (!$boolIsAllowedIp) {
					throw new SoapFault('Client',self::fnGetError(self::ERR_IP_ADDRESS_REJECTED)." [".long2ip($this->intIp)."]");
				}
			}
			//Load CompanyID
			$intCompanyId = (int)$this->zdb->fetchOne("SELECT CompanyID FROM Projects WHERE ProjectID = ".$intProjectId);
			Zend_Registry::set('CID',$intCompanyId);
			//load Bookkeeping Interface Handler Class!!!
			require_once("ibk_{$arrResult['Version']}.class.php");
			
		} else {
			throw new SoapFault("Client",self::fnGetError(self::ERR_PROJECT_TOKEN_ERROR));
		}
	}                                                                         
	
	/**
	* Gets the IP address of current client
	* @return void
	*/
	private function fnGetIp() {
		if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];	
		} else {
			$ip = $_SERVER['REMOTE_ADDR'];			
		}
		//  When viewed through an anonymous proxy, the address string
		// contans multiple ip#s separated hy commas. This fixes that.
		$ip_array = explode(",", $ip);
		$ip = $ip_array[count($ip_array)-1];
		$this->intIp = ip2long($ip);
	}
	
	/**
	* @param integer $intCode
	* @return string
	*/
	public static function fnGetError($intCode) {
		if (array_key_exists($intCode,self::$arrErrMsg)) {
			return $intCode."::".self::$arrErrMsg[$intCode];
		} else {
			return self::ERR_GENERAL."::".self::$arrErrMsg[self::ERR_GENERAL];
		}
	}
	
	/**
	* @param string $strVal
	*/
	private static function fnConvertOutputEncoding(&$strVal) {
		$strVal = iconv(self::$strEncoding,'UTF8',$strVal);
	}
	
	/**
	* @param string $strVal
	*/
	private static function fnConvertInputEncoding(&$strVal) {
		$strVal = iconv('UTF8',self::$strEncoding,$strVal);
	}
}