<?php
/**
* Class of Book-keeping Interface Handler for BMBY.Manage
* @package BMBY.Manage
* 
* @version 1.0.1
* 
* @since 05 May 2009 - 1.0.1	- Alex Sherman		- The BETA version is up on site, file name changed to include only the main version.
* @since 22 Apr 2009 - 1.0.0	- Alex Sherman 		- Original Interface Version
* 
* @author Alex Sherman [<alex@sherman-site.com>]
*/
require_once(INCLUDE_PATH . "lib/common.class.php");
require_once(INCLUDE_PATH . "lib/currency.class.php");
require_once(INCLUDE_PATH . "lib/contract.class.php");  
require_once(INCLUDE_PATH . "lib/contract_invoices.class.php");   
require_once(INCLUDE_PATH . "lib/contract_receipts.class.php");   

class CBookKeepingInterfaceHandler {
	/**
	* @return array List Of Company's Bookkeepings
	*/
	public static function fnGetBookkeepingList() {
		/**
		* @var Zend_Db_Adapter_Abstract
		*/		
		$zdb =  self::fnGetDbObject();
		$strSql = "SELECT 
				BookkeepingID,
				Num,
				Title,
				SoftwareType,
				Company,
				Prefix,
				TaxNo,
				CompanyNo,
				BankNo,
				BankName
			FROM 
				Bookkeeping 
			WHERE 
				CompanyID = ".(int)Zend_Registry::get('CID');

		return $zdb->fetchAssoc($strSql);
	}
	
	/**
	* @return array List of available payment methods for receipt details
	* @todo Add do API Reference
	*/
	public static function fnGetPaymentTypes() {
		/**
		* @var Zend_Db_Adapter_Abstract
		*/		
		$zdb =  self::fnGetDbObject();
		
		$strSql = "SELECT 
				MeanID as PymentTypeID,
				Title
			FROM 
				DefaultPaymentMeans
			ORDER BY 
				Priority";
		return $zdb->fetchAssoc($strSql);
	}
		
	/**
	* @return array
	*/
	public static function fnGetProjectCurrencies() {
		
		$intProjectID = (int)Zend_Registry::get('PID');
		$intCompanyID = (int)Zend_Registry::get('CID');
		$objCurrency = CCurrency::fnGetInstance($intProjectID,$intCompanyID);
		return $objCurrency->fnGetCurrenciesByProjectID("",$intProjectID);
	}
	
	/**
	* Get Contracts List for the project
	* 	
	* @param bool $intGetCanceledFlag - if true - get All contracts, including canceled. Default: true;
	* @return array
	*/
	public static function fnGetContracts($intGetCanceledFlag = 1) {
		
		$boolGetCanceled = (bool)$intGetCanceledFlag;
		
		$objContracts = new CContract();
		$arrParams['nolimit'] = true;
		$arrParams['tables'] = " LEFT JOIN CustomerTypes ON (Customers.CustomerTypeID = CustomerTypes.CustomerTypeID)";
		$strFields = "
		Contracts.CustomerID,
		Contracts.Address,
		IFNULL(Contracts.SalesPerson,0) as SalesPersonID,
		CustomerTypes.CustomerTypeID,
		CustomerTypes.Title as CustomerType,
		Contracts.ID as CompanyRegNum,
		Contracts.VATID as VatID,
		DATE_FORMAT(Contracts.SigningDate,'%d/%m/%Y') as ContractDate,
		DATE_FORMAT(Contracts.DeliveryDate,'%d/%m/%Y') as HandOverDate
		,Contracts.Remark as Comments
		";
		$arrParams['fields'] = $strFields.",0 AS Canceled";
		$arrContracts = $objContracts->fnGetContractsList($arrParams);
		
		if ($boolGetCanceled) {
			$arrParams['fields'] = $strFields.",1 AS Canceled";
			$arrContracsCanceled = $objContracts->fnGetContractsList($arrParams,true);
		} else {
			$arrContracsCanceled['contracts'] = array();
		}
		return array_merge($arrContracts['contracts'],$arrContracsCanceled['contracts']);
		
	}
	
	/**
	* Get Invoices
	* 
	* @param string $strFromDate
	* @param string $strToDate
	* @param string $strInvoiceType
	* @return array
	*/
	public static function fnGetInvoices($strFromDate = null,$strToDate = null,$strInvoiceType = null) {
        //Get ProjectID
		$intProjectID = (int)Zend_Registry::get('PID');
		//Validate Requested Invoice Type
		switch ($strInvoiceType) {
			case CContractInvoices::INVOICE_TYPE_ADVINVOICE:
			case CContractInvoices::INVOICE_TYPE_CREDIT:
			case CContractInvoices::INVOICE_TYPE_INVOICE:
			case CContractInvoices::INVOICE_TYPE_ANY:
				//No Action
				break;
			case null:
				//If not specified - defaults to ALL invoices
				$strInvoiceType = CContractInvoices::INVOICE_TYPE_ANY;
				break;
			default:
				//WRONG INVOICE TYPE
			 	throw new SoapFault('Client',CIbk::fnGetError(CIbk::ERR_WRONG_DOCUMENT_TYPE));
		}
		$arrParams = array();
		
		//From Date Filter
		if (is_null($strFromDate) || empty($strFromDate)) {
			$intFromDate = mktime(0,0,0,date('m'),1,date('Y'));
		} else {
			$intFromDate = CCommon::fnConvertDateToTimeStemp($strFromDate);
			if ($strFromDate !== date('d/m/Y',$intFromDate)) {
				throw new SoapFault('Client',CIbk::fnGetError(CIbk::ERR_WRONG_DATE_FORMAT));
			}
		}
		$arrParams['where'][] = "FI.PrintDate >= ".$intFromDate;
		//To Date Filter
		if (is_null($strToDate) || empty($strToDate)) {
			$intToDate = time();
		} else {
			$intToDate = CCommon::fnConvertDateToTimeStemp($strToDate,true);
			if ($strToDate !== date('d/m/Y',$intToDate)) {
				throw new SoapFault('Client',CIbk::fnGetError(CIbk::ERR_WRONG_DATE_FORMAT));
			}
		}
		$arrParams['where'][] = "FI.PrintDate <= ".$intToDate;
		//Get Invoices Data
		$objInvoicesHandler = new CContractInvoices();
		return $objInvoicesHandler->fnGetInvoices($intProjectID,$strInvoiceType,$arrParams);
	}
	
	/**
	* put your comment there...
	* 
	* @param string $strFromDate
	* @param string $strToDate
	* @param string $strReceiptType
	*/
	public static function fnGetReceipts($strFromDate = null,$strToDate = null,$strReceiptType = null) {
		$intProjectID = (int)Zend_Registry::get('PID');

		//Validate Requested Receipt Type
		switch ($strReceiptType) {
			case CContractReceipts::RECEIPT_TYPE_RECEIPT:
			case CContractReceipts::RECEIPT_TYPE_REFUND:
			case CContractReceipts::RECEIPT_TYPE_ANY:
				//No Action
				break;
			case null:
				//If not specified - defaults to ALL receipts
				$strReceiptType = CContractReceipts::RECEIPT_TYPE_ANY;
				break;
			default:
				//WRONG Receipt TYPE
			 	throw new SoapFault('Client',CIbk::fnGetError(CIbk::ERR_WRONG_DOCUMENT_TYPE));
		}
		$arrParams = array();
		
		//From Date Filter
		if (is_null($strFromDate) || empty($strFromDate)) {
			$intFromDate = mktime(0,0,0,date('m'),1,date('Y'));
		} else {
			$intFromDate = CCommon::fnConvertDateToTimeStemp($strFromDate);
			if ($strFromDate !== date('d/m/Y',$intFromDate)) {
				throw new SoapFault('Client',CIbk::fnGetError(CIbk::ERR_WRONG_DATE_FORMAT));
			}
		}
		$arrParams['where'][] = "FR.PrintDate >= ".$intFromDate;
		//To Date Filter
		if (is_null($strToDate) || empty($strToDate)) {
			$intToDate = time();
		} else {
			$intToDate = CCommon::fnConvertDateToTimeStemp($strToDate,true);
			if ($strToDate !== date('d/m/Y',$intToDate)) {
				throw new SoapFault('Client',CIbk::fnGetError(CIbk::ERR_WRONG_DATE_FORMAT));
			}
		}
		$arrParams['where'][] = "FR.PrintDate <= ".$intToDate;
				
		//Get Invoices Data
		$objInvoicesHandler = new CContractReceipts();
		return $objInvoicesHandler->fnGetReceiptsIbk($intProjectID,$strReceiptType,$arrParams);
	}

	/**
	* Add new Receipt/Refund
	* 
	* @param integer $intBookkeepingID
	* @param integer $intContractID
	* @param string $strReceiptType ENUM 
	* @param string $strReceiptDate
	* @param integer $intCurrencyID
	* @param float $fltSum
	* @param string $strReference3rdParty
	* @param string $strRemark Optional Comment
	* @return integer FinReceiptID
	*/
	public static function fnAddReceipt($intBookkeepingID,$intContractID,$strReceiptType,$strReceiptDate,$intCurrencyID,$fltSum,$fltRate,$strReference3rdParty,$strRemark = null) {
		/**
		* @var Zend_Db_Adapter_Abstract
		*/
		$zdb = self::fnGetDbObject();
		$intProjectID = (int)Zend_Registry::get('PID');
		
		
		//Check bookkeeping is valid for this project
		$intBookkeepingID = (int)$intBookkeepingID;
		$arrBookkeeping = self::fnGetBookkeepingList();
		if (!array_key_exists($intBookkeepingID,$arrBookkeeping)) {
			throw new SoapFault('Client',CIbk::fnGetError(Cibk::ERR_WRONG_BOOKKEEPING_ID));
		}


		//Check ContractID is valid for this project
		$strSql = "select ContractID from Contracts WHERE ProjectID = {$intProjectID} AND ContractID = ".(int)$intContractID." LIMIT 1";
		$intContractDbID = (int)$zdb->fetchOne($strSql);
		if (empty($intContractDbID) || $intContractDbID != $intContractID) {
			throw new SoapFault('Client',Cibk::fnGetError(Cibk::ERR_WRONG_CONTRACT_ID));
		}
		
		//Check Receipt Type
		$arrAllowedTypes = array(CContractReceipts::RECEIPT_TYPE_RECEIPT,CContractReceipts::RECEIPT_TYPE_REFUND);
		if (!in_array($strReceiptType,$arrAllowedTypes)) {
			throw new SoapFault('Client',CIbk::fnGetError(CIbk::ERR_WRONG_DOCUMENT_TYPE));
		}

		//Check 3rd Party Reference
		if (empty($strReference3rdParty)) {
			throw new SoapFault('Client',CIbk::fnGetError(CIbk::ERR_REFRENCE_REQUIRED));
		}
		$strSql = "SELECT FinReceiptID FROM FinReceipts WHERE IsCanceled = 0 AND BookkeepingID = {$intBookkeepingID} AND Reference3rdParty = ".$zdb->quote($strReference3rdParty);
		$boolAlreadyProccessedReference = (bool)$zdb->fetchOne($strSql);
		if ($boolAlreadyProccessedReference) {
			throw new SoapFault('Client',CIbk::fnGetError(CIbk::ERR_REFERENCE_IN_USE));
		}
		
		
		//Validate receipt date
		$intReceiptDate = CCommon::fnConvertDateToTimeStemp($strReceiptDate);
		if ($strReceiptDate != date('d/m/Y',$intReceiptDate)) {
			throw new SoapFault('Client',CIbk::fnGetError(CIbk::ERR_WRONG_DATE_FORMAT));
		}
		
		//Check currecny ID 
		$arrCurrencies = self::fnGetProjectCurrencies();
		if (!array_key_exists($intCurrencyID,$arrCurrencies)) {
			throw new SoapFault('Client',CIbk::fnGetError(CIbk::ERR_WRONG_CURRENCY_ID));
		}
		
		//Check Sum
		$fltSum = (float)$fltSum;
		if ($fltSum <= 0) {
			throw new SoapFault('Client',CIbk::fnGetError(CIbk::ERR_WRONG_POSITIVE_SUM));
		}
		
		//Check Currency Rate
		$fltRate = (float)$fltRate;
		if ($fltRate <= 0) {
			throw new SoapFault('Client',CIbk::fnGetError(CIbk::ERR_WRONG_CURRENCY_RATE));
		}

		//Build data array for DB
		$arrData['NewLostAtribute'] = 1;
		$arrData['Type'] = $strReceiptType;
		$arrData['LostAtributeContractID'] = $intContractID;
		$arrData['BookkkepingID'] = $intBookkeepingID;
		$arrData['RedemptionDate'] = $intReceiptDate;
		$arrData['LostAtributeSumIncVat'] = $fltSum;
		$arrData['LostAtributeCurrencyID'] = $intCurrencyID;
		$arrData['LostAtributeRate'] = $fltRate;
		$arrData['Reference3rdParty'] = $strReference3rdParty;
		
		if (!empty($strRemark)) {
			$arrData['Remark'] = $strRemark;
		}
		
		if(!$zdb->insert('FinReceipts',$arrData)) {
			throw new SoapFault('Server',CIbk::fnGetError(CIbk::ERR_FAILED_TO_WRITE));
		}
		return (int)$zdb->lastInsertId('FinReceipts');
	}
	
	/**
	* put your comment there...
	* 
	* @param mixed $intReceiptID
	* @param mixed $intPaymentTypeID
	* @param mixed $fltSum
	* @param mixed $strRedemptionDate
	* @param mixed $strCheckNum
	* @param mixed $strBank
	* @param mixed $strBankBranch
	* @param mixed $strBankAccount
	* @return mixed
	*/
	public static function fnAddReceiptPayment($intReceiptID,$intPaymentTypeID,$fltSum,$strRedemptionDate,$strCheckNum="",$strBank="",$strBankBranch="",$strBankAccount="") {
		
		/**
		* @var Zend_Db_Adapter_Abstract
		*/
		$zdb = self::fnGetDbObject();
		
		//Check ReceiptID & Sum
		$intReceiptID = (int)$intReceiptID;
		$strSql = "SELECT FinReceiptID,LostAtributeSumIncVat FROM FinReceipts Where IsCanceled = 0 AND NewLostAtribute=1 AND FinReceiptID = ".$intReceiptID;
		$arrReceiptData = $zdb->fetchRow($strSql);
		if (empty($arrReceiptData)) {
			throw new SoapFault('Client',CIbk::fnGetError(CIbk::ERR_NOT_VALID_RECEIPT_ID));
		}
		
		$fltSum = (float)$fltSum;
		if (empty($fltSum)) {
			throw new SoapFault('Client',CIbk::fnGetError(CIbk::ERR_WRONG_POSITIVE_SUM));
		}
		$strSql ="SELECT IFNULL(SUM(RcptSumIncVat),0) as RCPT_SUM FROM FinReceiptPayments Where FinReceiptID = ".$intReceiptID;
		$fltTotalExistingPayments = (float)$zdb->fetchOne($strSql);
		$fltTotalReceiptSum = (float)$arrReceiptData['LostAtributeSumIncVat'];
		if (($fltTotalReceiptSum - $fltTotalExistingPayments) < $fltSum) {
			throw new SoapFault('Client',CIbk::fnGetError(CIbk::ERR_PAYMENT_EXCEEDS_RCPT));
		}
		
		//Check Payment Type
		$arrPaymentTypes = self::fnGetPaymentTypes();
		if (!array_key_exists($intPaymentTypeID,$arrPaymentTypes)) {
			throw new SoapFault('Server',CIbk::fnGetError(CIbk::ERR_WRONG_PAYMENT_TYPE_ID));
		}
		
		//Validate redemption date
		$intRedemptionDate = CCommon::fnConvertDateToTimeStemp($strRedemptionDate);
		if ($strRedemptionDate != date('d/m/Y',$intRedemptionDate)) {
			throw new SoapFault('Client',CIbk::fnGetError(CIbk::ERR_WRONG_DATE_FORMAT));
		}
		
		$arrData['FinReceiptID'] = $intReceiptID;
		$arrData['RcptSumIncVat'] = $fltSum;
		$arrData['MeanID'] = $intPaymentTypeID;
		$arrData['RedemptionDate'] = $intRedemptionDate;
		
		if (!empty($strCheckNum)) {
			$arrData['CheckNum'] = $strCheckNum;
		}
		if (!empty($strBank)) {
			$arrData['Bank'] = $strBank;
		}
		if (!empty($strBankBranch)) {
			$arrData['BankBranch'] = $strBankBranch;
		}
		if (!empty($strBankAccount)) {
			$arrData['BankAccount'] = $strBankAccount;
		}
		
		
		if(!$zdb->insert('FinReceiptPayments',$arrData)) {
			throw new SoapFault('Server',CIbk::fnGetError(CIbk::ERR_FAILED_TO_WRITE));
		}
		return (int)$zdb->lastInsertId('FinReceiptPayments');
	}
	
	/**
	* @return Zend_Db_Adapter_Abstract
	*/
	private static function fnGetDbObject() {
		$zdb = Zend_Registry::get('zdb');
		if ($zdb instanceof Zend_Db_Adapter_Abstract) {
			return $zdb;
		} else {
			throw new SoapFault('Server',CIbk::fnGetError(CIbk::ERR_NO_DB));
		}
	}
}

