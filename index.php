<?php
/**
* IBK = Interface Book Keeping
* @since - 2009-Apr-12 - Created
*/

require_once("config.Yielding.php");
require_once(INCLUDE_PATH ."conn.php");
require_once(INCLUDE_PATH ."lib/common.class.php");
require_once("lib/ibk.class.php");

$server = new SoapServer(null,array("uri"=>"http://{$_SERVER['SERVER_NAME']}/manage/IBK/"));
$server->setClass('CIbk', new CIbk());
$server->handle();   